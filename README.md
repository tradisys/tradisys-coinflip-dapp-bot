# Tradisys Coin Flip dAapp bot

This is simple UI-bot, which allows to play and follow for the yours leaderboard movement!

## Customization
First go to the `src/config.js` and set `dApp` (decentralized application address). You can receive it
using https://www.coin-flip.io/, click to the any game in the `GAME HISTORY` table and then button `Smart Contract`.
On the opened page you will see address of this contract. Copy and paste it here.
```
  dapp: {
    address: '3ND68eBy9NyJPeq4eRqi42c45hoDAzzRjSm',
  },
```
Then you can begin project setup. After launch, go to the `http://localhost:8080/ ` (port may be different), and
enter your seed phrase, your address will defined automatically. Finally press `start` button.
Good luck!

## Environment setup
1. Download latest https://nodejs.org/en/
2. Install with default settings
3. Download this repository as a zip-archive or use `git clone` command to fetch sources
4. Open terminal on your OS and check working directory - it must be project folder
5. Complete project setup

## Project setup
Enter following commands to complete setup:
```
npm install
```
And then launch web local web server:
```
npm run serve
```

In the terminal output your will wee something like:
```                                                            11:15:41 AM                                                                                                               
     App running at:
     - Local:   http://localhost:8080/ 
     - Network: http://192.168.1.1:8080/
```
Open your web browser and enter the local address `http://localhost:8080/`
