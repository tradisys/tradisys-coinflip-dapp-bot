import urlJoin from 'url-join';
import config from '@/config';
import { jsonHeaders } from '@/utils';

const nodeUrl = config.api.node.url;

export const broadcastTx = async (tx) => {
  const response = await fetch(urlJoin(
    nodeUrl,
    '/transactions',
    '/broadcast',
  ), {
    method: 'POST',
    headers: jsonHeaders,
    body: tx,
  });

  return response.json();
};

export const getGameResultById = async (
  id,
) => {
  const response = await fetch(urlJoin(
    nodeUrl,
    '/addresses',
    '/data',
    config.dapp.address,
    id,
  ), {
    method: 'GET',
    headers: jsonHeaders,
  });

  return response.json();
};

export const getAddressBalanceInfo = async (
  address,
) => {
  const response = await fetch(urlJoin(
    nodeUrl,
    '/addresses',
    '/balance',
    '/details',
    address,
  ), {
    method: 'GET',
    headers: jsonHeaders,
  });

  return response.json();
};

export const getStatsLeaderboard = async () => {
  try {
    const response = await fetch(urlJoin(
      config.api.backend.url,
      config.api.backend.statsGameEndpoint,
      `/leaders?gameName=${config.api.gameId}&limit=100`,
    ), {
      method: 'GET',
      headers: jsonHeaders,
    });

    return response.json();
  } catch (e) {
    return [];
  }
};
