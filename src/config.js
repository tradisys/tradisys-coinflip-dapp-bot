// Application config
//
// Environment vars should be defined by following method:
// https://cli.vuejs.org/guide/mode-and-env.html

export default {
  app: {
    name: 'Coin Flip Dapp Playing Bot',
  },
  api: {
    network: 'T',
    gameId: 'COIN_FLIP',
    node: {
      url: 'https://testnodes.wavesnodes.com',
      data: '/addresses/data',
      transactionById: '/transactions/info',
      broadcast: '/transactions/broadcast',
      balance: '/addresses/balance/details',
    },
    backend: {
      url: 'https://gamesapi.tradisys.com/api',
      startGameEndpoint: '/game/start',
      statsGameEndpoint: '/stats',
      suspendedGamesEndpoint: '/suspended',
      gameInfoEndpoint: '/info',
      gameSourcesEndpoint: '/sources',
      replicateGameEndpoint: '/replicate',
    },
  },
  wave: 100000000,
  dapp: {
    address: '3ND68eBy9NyJPeq4eRqi42c45hoDAzzRjSm',
  },
};
